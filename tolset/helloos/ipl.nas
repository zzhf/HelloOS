; hello-os
; TAB=4

		ORG		0x7c00			;指明程序的装?地址

;以下的?叙用于?准FAT16格式的U?

		JMP		entry
		DB		0x90
		DB		"OSIPL1.0"		; ??区的名称（8字?）
		DW		512				; ?个扇区的大小（必??512字?）
		DB		2				; 簇的大小（FAT16?2）
		DW		2				; 保留的扇区（FAT16?2）
		DB		2				; FAT的个数
		DW		512				; 根目?的大小512
		DW		0				; 扇区数（低于32MB）
		DB		0xF8			; 媒体描述（16?制）
		DW		247				; ?个FAT的扇区数
		DW		63				; ?个磁道的扇区数
		DW		255				; 磁?数
		DD		256				; ?藏扇区数
		DD		126592			; 扇区数（超?32MB）
		DB		0x80,0x00,0x29	; BIOS??,保留不用，?展???名
		DD		0xA80F20AE		; 卷?号?（原名Volume serial number）
		DB		"HELLO-OS   "	; 卷名，磁?的名称
		DB		"FAT16   "		; フォーマットの名前（8バイト）
		RESB	18				; とりあえず18バイトあけておく

; プログラム本体

entry:
		MOV		AX,0			; レジスタ初期化
		MOV		SS,AX
		MOV		SP,0x7c00
		MOV		DS,AX
		MOV		ES,AX

		MOV		SI,msg
putloop:
		MOV		AL,[SI]
		ADD		SI,1			; SIに1を足す
		CMP		AL,0
		JE		fin
		MOV		AH,0x0e			; 一文字表示ファンクション
		MOV		BX,15			; カラーコード
		INT		0x10			; ビデオBIOS呼び出し
		JMP		putloop
fin:
		HLT						; 何かあるまでCPUを停止させる
		JMP		fin				; 無限ループ

msg:
		DB		0x0a, 0x0a		; 改行を2つ
		DB		"hello, world"
		DB		0x0a			; 改行
		DB		0

		RESB	0x7dfe-$		; 0x7dfeまでを0x00で埋める命令

		DB		0x55, 0xaa		; ??区制作完成

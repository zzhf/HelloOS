# 30天自制操作系统

#### 项目介绍
学习川合秀实的30天自制操作系统（学习基础）

开始自己的操作系统制作：
将书中的软盘启动方式更改为U盘启动方式(64MB)
首先将U盘格式化为FAT16格式
将U盘插入电脑
管理员权限运行!cons_nt.bat
运行make命令生成启动扇区文件ipl.bin
运行ShowAllDisk.exe确认U盘对应的驱动器号（0,1,2...)一般都是最后的那个盘
运行AbsDiskWrite.exe ipl.bin 驱动器号将bin文件写入U盘的启动扇区
运行ShowAllDisk.exe检查启动扇区是否写入
插入真机以U盘方式启动